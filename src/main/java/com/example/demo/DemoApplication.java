package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {
	//https://blog.csdn.net/aiyo92/article/details/101064167/
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
