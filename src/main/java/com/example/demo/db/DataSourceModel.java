package com.example.demo.db;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Hyman.huang
 * @create 2023-07-13-17:27
 */
@Data
@AllArgsConstructor
public class DataSourceModel {
    private String username;
    private String password;
    private String dbType;
    private String url;

}
