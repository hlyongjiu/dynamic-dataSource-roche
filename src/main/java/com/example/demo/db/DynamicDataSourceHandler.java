package com.example.demo.db;

/**
 * @author Hyman.huang
 * @create 2023-07-17-9:19
 */
public class DynamicDataSourceHandler {
    private static final ThreadLocal<String> DATASOURCE = new ThreadLocal<>();

    public static void setDataSource(String dataSource) {
        DATASOURCE.set(dataSource);
    }

    public static String getDataSource() {
        return DATASOURCE.get();
    }

    public static void clearDataSource() {
        DATASOURCE.remove();
    }
}
