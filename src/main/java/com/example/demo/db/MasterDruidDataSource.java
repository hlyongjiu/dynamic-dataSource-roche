package com.example.demo.db;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;

/**
 * @author Hyman.huang
 * @create 2023-07-14-13:22
 */
@ConditionalOnClass(DynamicDataSource.class)
public class MasterDruidDataSource extends DruidDataSource {

}
