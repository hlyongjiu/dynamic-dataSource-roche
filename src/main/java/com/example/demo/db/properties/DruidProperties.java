package com.example.demo.db.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Hyman.huang
 * @create 2023-07-18-10:24
 */
@Data
public abstract class DruidProperties {
    // 连接池初始化大小
    protected int initialSize;

    // 连接池最小值
    protected int minIdle;

    // 连接池最大值
    protected int maxActive;

    // 配置获取连接等待超时的时间
    protected int maxWait;

    // 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
    protected int timeBetweenEvictionRunsMillis;

    // 配置一个连接在池中最小生存的时间，单位是毫秒
    protected int minEvictableIdleTimeMillis;

    // 用来验证数据库连接的查询语句,这个查询语句必须是至少返回一条数据的SELECT语句
    protected String validationQuery;

    // 检测连接是否有效
    protected boolean testWhileIdle;

    // 申请连接时执行validationQuery检测连接是否有效。做了这个配置会降低性能。
    protected boolean testOnBorrow;

    // 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能
    protected boolean testOnReturn;

    // 是否缓存preparedStatement，也就是PSCache。
    protected boolean poolPreparedStatements;

    // 指定每个连接上PSCache的大小。
    protected int maxPoolPreparedStatementPerConnectionSize;
}
