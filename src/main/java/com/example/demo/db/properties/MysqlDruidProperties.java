package com.example.demo.db.properties;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Hyman.huang
 * @create 2023-07-18-10:10
 */
@Component
@ConfigurationProperties(prefix = "db.dynamic.mysql.druid")
public class MysqlDruidProperties extends DruidProperties {


}
