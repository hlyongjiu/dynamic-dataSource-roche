package com.example.demo.db.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Hyman.huang
 * @create 2023-07-18-10:10
 */
@ConfigurationProperties(prefix = "db.dynamic.sqlserver.druid")
public class SqlServerDruidProperties extends DruidProperties{


}
