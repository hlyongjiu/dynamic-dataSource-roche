package com.example.demo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Hyman.huang
 * @create 2023-07-13-12:55
 */
@Data
public class BaseReq {
    /**
     * 数据库连接类型：1:mysql，2:sqlserver，3:oracle等
     **/
    @ApiModelProperty(value = "数据库连接类型", required = true)
    @NotNull
    private Integer dbType;

    /** 连接地址 http://localhost:3306/mybatis?characterEncoding=utf-8 **/
    @ApiModelProperty(value = "连接地址", required = true)
    @NotBlank
    private String url;

    @ApiModelProperty(value = "数据库连接账户", required = true)
    @NotBlank
    private String username;

    @ApiModelProperty(value = "数据库连接密码", required = true)
    @NotBlank
    private String password;

    /** 操作类型：可通过枚举转义 1：select，2：insert，3：update，4：delete **/
    @ApiModelProperty(value = "操作类型", required = true)
    private Integer operateType;

    /** sql 语句，多个以逗号分割 **/
    @ApiModelProperty(value = "操作类型", required = true)
    private List<String> sqlList;

//    private String jsonResult;

//    /** 操作结果类型 1:int 2:object 3:list**/
//    private Integer resultType;
//
//    /** 操作结果，如果是select，需要满足什么结果再做返回成功处理 **/
//    private Integer expectedResults;
}
