package com.example.demo.dto;

import lombok.Data;

/**
 * @author Hyman.huang
 * @create 2023-07-13-14:04
 */
@Data
public class RR<T> {
    private int code;
    private String msg;
    private T data;
}
