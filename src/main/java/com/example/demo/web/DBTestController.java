package com.example.demo.web;

import com.example.demo.dto.BaseReq;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author Hyman.huang
 * @create 2023-07-13-13:53
 */
@RestController("/db")
public class DBTestController {


    @PostMapping("/test1")
    public void test1(@RequestBody @Valid BaseReq req) {
        //
    }
}
