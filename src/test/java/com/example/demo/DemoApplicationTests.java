package com.example.demo;

import com.baomidou.dynamic.datasource.toolkit.DynamicDataSourceContextHolder;
import com.example.demo.db.*;
import com.example.demo.db.properties.MysqlDruidProperties;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@SpringBootTest
//@EnableConfigurationProperties(MysqlDruidProperties.class)
class DemoApplicationTests {

    @Autowired
    DataSource dynamicDataSource;
    @Autowired
    MysqlDruidProperties mysqlDruidConfig;
    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 动态数据源测试
     *
     * @throws SQLException
     */
    @Test
    void contextLoads() throws SQLException {

        /*System.out.println(mysqlDruidConfig);
        System.out.println(dataSource.getClass());
        Connection connection = dataSource.getConnection();
        System.out.println(connection);
        connection.close();*/
        Object integer = jdbcTemplate.queryForObject("select count(*) from mysql_test", Object.class);
        System.out.println("第一次查询+"+integer);


        //新启动线程创建数据源
        DataSourceModel dataSourceModel = new DataSourceModel("root", "P@ssw0rd", "mysql", "jdbc:mysql://47.92.158.112:3306/ry-vue?allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=utf8&useSSL=false");
        Thread th = new Thread(() -> {
            System.out.println("DDDDDDDDDD");

            //判断数据源是否有数据
            if (!DynamicDataSource.dynamicTargetDataSources.containsKey(dataSourceModel.getUrl())) {
                DynamicDataSource dynamicDataSource = new DynamicDataSource();
                dynamicDataSource.createDataSource(dataSourceModel.getUrl(), dataSourceModel.getUsername(), dataSourceModel.getPassword(), dataSourceModel.getDbType());
            }
            DynamicDataSourceHandler.setDataSource(dataSourceModel.getUrl());
            //jdbcTemplate.setDataSource(DynamicDataSourceHandler.getDataSource());
            try {
                System.out.println("dfsdfsdf333333333333333333333");
                Integer integer1 = jdbcTemplate.queryForObject("select count(*) from sys_oper_log", Integer.class);

            } catch (Exception e) {
                System.out.println(e);
            }

            // System.out.println("mysql 查询结果" + integer1);
            //DynamicDataSourceHandler.clearDataSource();
        });
        th.start();


        try {
            Thread.sleep(600000L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }

}
